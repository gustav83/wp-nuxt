# wp-nuxt

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Wordpress setup

**Required plugins**
- wp-rest-api-v2-menus
- advanced-custom-fields-pro
- acf-to-rest-api

**Recommended plugins**
- wp-api-yoast-meta

## ACF-REST setup
```
<?php

// Add options page
if(function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'General Settings',
        'menu_title' => 'Settings',
        'menu_slug'  => 'general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
        'post_id'    => 'general-settings'
    ));
}

// Enable the option show in rest
add_filter('acf/rest_api/field_settings/show_in_rest', '__return_true');

// Enable the option edit in rest
add_filter('acf/rest_api/field_settings/edit_in_rest', '__return_true');
```



## TODO
- autodescription API (The SEO Framework plugin)
- Gravityforms
- Post-types
- Lists
