module.exports = {
  /*
  ** Environment
  */
  env: {
    API_URL: process.env.API_URL || 'http://localhost/wp-json',
  },
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s | wp-nuxt',
    htmlAttrs: { lang: 'en' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico?v=1.1' },
    ],
  },
  /*
  ** Load global CSS
  */
  css: ['@/assets/css/main.css'],
  /*
  ** This option is given directly to the vue-router Router constructor
  */
  router: {
    base: '',
    linkActiveClass: 'is-active',
    extendRoutes(routes) {
      // Frontpage
      routes.push({
        name: 'start',
        path: '/',
        component: __dirname + '/pages/index.vue',
      })
      // Add custom routes here
      // routes.push(..)
      // Load from WP-slug
      routes.push({
        name: 'custom',
        path: '*',
        component: __dirname + '/pages/_slug/index.vue',
      })
    }
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Plugins
  */
  plugins: [
    { src: '~/plugins/tawk', ssr: false },
  ],
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios'],
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      // Remove .svg from urlLoader
      const urlLoader = config.module.rules.find((rule) => rule.loader === 'url-loader')
      urlLoader.test = /\.(png|jpe?g|gif)$/

      // SVG-loader
      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
        exclude: /(node_modules)/,
        options: {
          svgo: {
            plugins: [
              { removeDoctype: true },
              { removeComments: true }
            ]
          }
        }
      })
    }
  }
}
