import axios from 'axios'

export default {
  baseUrl: process.env.API_URL,

  getOptions(slug) {
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + '/acf/v3/options/' + encodeURIComponent(slug)
      axios.get(url).then(response => {
        if (response.status === 200) {
          resolve(response)
        } else {
          reject(response)
        }
      })
    })
  },

  getPage(slug) {
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + '/wp/v2/pages?slug=' + encodeURIComponent(slug)
      axios.get(url).then(response => {
        if(response.status === 200 && response.data.length > 0) {
          response.data = response.data[0]
          resolve(response)
        } else {
          reject(response)
        }
      })
    })
  },

  getMenu(slug) {
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + '/menus/v1/menus/' + slug
      return axios.get(url).then(response => {
        if(response.status === 200) {
          response.data = response.data.items.map(item => {
            return {
              id: item.ID,
              title: item.title,
              url: item.url.indexOf('://') > -1 ? item.url.split(/\//, 4)[3] : item.url,
              _obj: item,
            }
          })
          resolve(response)
        } else {
          reject(response)
        }
      })
    })
  }
}
