import Vuex from 'vuex'
import api from '~/api'

const createStore = () => {
  return new Vuex.Store({
    state: {
      config: {},
      page: {},
      primaryMenu: [],
    },
    actions: {
      async nuxtServerInit({dispatch}) {
        await dispatch('getConfig')
        await dispatch('getPrimaryMenu')
      },
      getConfig({commit}) {
        return new Promise((resolve, reject) => {
          api.getOptions('general-settings').then(
            response => {
              commit('setConfig', response.data.acf)
              resolve(response.data)
            },
            response => reject
          )
        })
      },
      getPage({commit}, slug) {
        return new Promise((resolve, reject) => {
          api.getPage(slug).then(
            response => {
              commit('setPage', response.data)
              resolve(response.data)
            },
            response => reject
          )
        })
      },
      getPrimaryMenu({commit}) {
        return new Promise((resolve, reject) => {
          api.getMenu('primary').then(
            response => {
              commit('setPrimaryMenu', response.data)
              resolve(response.data)
            },
            reject => reject
          )
        })
      },
    },
    mutations: {
      setConfig(state, data) {
        state.config = data
      },
      setPage: (state, data) => {
        state.page = data
      },
      setPrimaryMenu: (state, data) => {
        state.primaryMenu = data
      },
    },
    getters: {
      config(state) {
        return key => {
          return key in state.config ? state.config[key] : null
        }
      }
    },
  })
}

export default createStore
